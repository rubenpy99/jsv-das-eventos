//a. Imprimir en la consola cada valor usando "for"

for (let i = 0; i < INGE3.length; i++) {
  console.log(INGE3[i]);
}

//b. Idem al anterior usando "forEach"

INGE3.forEach(function(valor) {
  console.log(valor);
});

//c. Idem al anterior usando "map"

INGE3.map(function(valor) {
  console.log(valor);
});

//d. Idem al anterior usando "while"

let i = 0;
while (i < INGE3.length) {
  console.log(INGE3[i]);
  i++;
}

//e. Idem al anterior usando "for..of"

for (let valor of INGE3) {
  console.log(valor);
}
//declare un vector de 6 elementos, usando 3 tipos de datos JS

let INGE3 = [34, 'Texto1', true, 12.5, false, 'Texto2'];

//a. Imprimir en la consola el vector

for (let i = 0; i < INGE3.length; i++) {
  console.log(INGE3[i]);
}

//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log(INGE3[0]);
console.log(INGE3[INGE3.length - 1]);
//c. Modificar el valor del tercer elemento
INGE3[2] = false;
//d. Imprimir en la consola la longitud del vector
console.log(INGE3.length);
//e. Agregar un elemento al vector usando "push"
INGE3.push('NuevoElemento');
//f. Eliminar elemento del final e imprimirlo usando "pop"
let ultimoElemento = INGE3.pop();
console.log(ultimoElemento);
//g. Agregar un elemento en la mitad del vector usando "splice"
let mitad = Math.ceil(INGE3.length / 2);
INGE3.splice(mitad, 0, 'NuevoElemento');
//h. Eliminar el primer elemento usando "shift"
let primerElemento = INGE3.shift();
//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
INGE3.unshift(primerElemento);
console.log(INGE3);